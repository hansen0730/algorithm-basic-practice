#include <stdio.h>

int max_sum(int *arr, int length) {
    int i;

    int max_sum = -100000;
    int sum = 0;
    for (i = 0; i < length; i++) {
        if (sum < 0) {
            sum = arr[i];
        } else {
            sum += arr[i];
        }

        if (max_sum < sum) {
            max_sum = sum;
        }
    }

    return max_sum;
}

int main() {
    int arr[] = { 1, -2, 3, 10, -4, 7, 2, -5, 6 };
    int length = sizeof(arr) / sizeof(int);
    printf("length is %d\n", length);
    printf("max sum is %d\n", max_sum(arr, length));

    return 0;
}
