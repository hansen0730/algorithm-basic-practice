#include <stdio.h>
#include <stdlib.h>

int fabonacci(int n) {
    if (n == 0 || n == 1) {
        return n;
    }

    int first = 0;
    int second = 1;
    int result;

    int i;
    for (i = 2; i <= n; i++) {
        result = first + second;
        first = second;
        second = result;
    }

    return result;
}

int main() {
    int i;
    for (i = 0; i < 10; i++) {
        printf("result is %d\n", fabonacci(i));
    }

    return 0;
}
