#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct ListNode {
    int key;
    struct ListNode *next;
} ListNode;

ListNode * initList(int length) {
    srand(time(NULL));
    ListNode *head = (ListNode*) malloc (sizeof(ListNode));
    head->key = rand() % 100;
    ListNode *prev = head;
    ListNode *curr;

    int i;
    for (i = 1; i < length; i++) {
        curr = (ListNode*) malloc (sizeof(ListNode));
        curr->key = rand() % 100;
        prev->next = curr;
        prev = curr;
    }
    prev->next = NULL;

    return head;
}

void printList(ListNode *head) {
    ListNode *curr = head;
    while (curr) {
        printf("%d ", curr->key);
        curr = curr->next;
    }
    printf("\n");
}

ListNode* reverseList(ListNode *head) {
    ListNode *reverseHead = NULL;
    ListNode *node = head;
    ListNode *prev = NULL;

    while (node != NULL) {
        ListNode *next = node->next;

        if (next == NULL) {
            reverseHead = node;
        }

        node->next = prev;
        prev = node;
        node = next;
    }

    return reverseHead;
}

int main() {
    int length = 10;
    ListNode *head = initList(length);
    printList(head);
    ListNode *reverse = reverseList(head);
    printList(reverse);

    return 0;
}
