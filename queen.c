#include <stdio.h>
#include <stdlib.h>

#define N 8

int queen[N];
int sum = 0;

void output() {
    int i;
    for (i = 0; i < N; i++) {
        printf("(%d %d) ", i, queen[i]);
    }
    printf("\n");
    sum++;
}

int check(int col) {
    int i;
    for (i = 0; i < col; i++) {
        if (queen[i] == queen[col] || abs(queen[i] - queen[col]) == (col - i)) {
            return 0;
        }
    }

    return 1;
}

void put(int row) {
    int i, j;
    for (i = 0; i < N; i++) {
        queen[row] = i;
        if (check(row)) {
            if (row == N - 1) {
                output();
            } else {
                put(row + 1);
            }
        }
    }
}

int main() {
    put(0);
    printf("sum is %d\n", sum);

    return 0;
}
